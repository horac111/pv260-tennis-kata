namespace Tennis
{
    public class TennisGame2 : ITennisGame
    {
        private int p1point { get; set; }
        private int p2point { get; set; }

        private readonly string player1Name;
        private readonly string player2Name;

        public TennisGame2(string player1Name, string player2Name)
        {
            this.player1Name = player1Name;
            p1point = 0;
            p2point = 0;
            this.player2Name = player2Name;
        }

        public string GetScore()
        {
            string p1res = "";
            string p2res = "";
            var score = "";
            if (p1point == p2point && p1point < 3)
            {
                score = ScoreToString(p1point);
                    
                score += "-All";
            }
            else if (p1point == p2point && p1point > 2)
            {
                score = "Deuce";
            }
            else if (p1point < 4 && p2point < 4)
            {
                p1res = ScoreToString(p1point);
                p2res = ScoreToString(p2point);

                score = p1res + "-" + p2res;
            }
            else if (p1point >= 4 && p2point >= 0 && (p1point - p2point) >= 2)
            {
                score = "Win for player1";
            }
            else if (p2point >= 4 && p1point >= 0 && (p2point - p1point) >= 2)
            {
                score = "Win for player2";
            }
            else if (p1point > p2point && p2point >= 3)
            {
                score = "Advantage player1";
            }
            else 
            {
                score = "Advantage player2";
            }
            
            return score;
        }

        private string ScoreToString(int point)
        {
            switch (point)
            {
                case 0:
                    return "Love";
                case 1:
                    return "Fifteen";
                case 2:
                    return "Thirty";
                case 3:
                    return "Forty";
                default:
                    return "";
            }
        }

        public void AddP1Score(int number)
        {
            p1point += number;
        }

        public void AddP2Score(int number)
        {
            p2point += number;
        }

        public void WonPoint(string player)
        {
            if (player == player1Name)
                AddP1Score(1);
            else
                AddP2Score(1);
        }

    }
}

