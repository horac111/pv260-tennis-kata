namespace Tennis
{
    public class TennisGame1 : ITennisGame
    {
        private int m_score1 = 0;
        private int m_score2 = 0;
        private readonly string player1Name;
        private readonly string player2Name;

        public TennisGame1(string player1Name, string player2Name)
        {
            this.player1Name = player1Name;
            this.player2Name = player2Name;
        }

        public void WonPoint(string playerName)
        {
            if (playerName == player1Name)
                m_score1 += 1;
            else
                m_score2 += 1;
        }

        public string GetScore()
        {
            if (m_score1 >= 4 || m_score2 >= 4)
            {
                var minusResult = m_score1 - m_score2;
                if (minusResult == 0) 
                    return "Deuce";
                else if (minusResult == 1)
                    return "Advantage player1";
                else if (minusResult == -1)
                    return "Advantage player2";
                else if (minusResult >= 2)
                    return "Win for player1";
                else 
                    return "Win for player2";
            }

            if (m_score1 == m_score2)
            {
                return ScoreToString(m_score1, true);
            }

            string score = ""; 
            score = ScoreToString(m_score1, false) + "-";
            score += ScoreToString(m_score2, false);
            return score;
        }

        private string ScoreToString(int point, bool same)
        {
            string score = "";
            switch (point)
            {
                case 0:
                    score = "Love";
                    break;
                case 1:
                    score = "Fifteen";
                    break;
                case 2:
                    score = "Thirty";
                    break;
                case 3:
                    if (same)
                        score = "Deuce";
                    else
                        score = "Forty";
                    break;
            }
            if (same && point != 3)
            {
                score += "-All";
            }
            return score;
        }
    }
}

