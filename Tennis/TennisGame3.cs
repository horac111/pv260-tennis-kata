namespace Tennis
{
    public class TennisGame3 : ITennisGame
    {
        private int p2Score;
        private int p1Score;
        private readonly string p1Name;
        private readonly string p2Name;

        public TennisGame3(string player1Name, string player2Name)
        {
            this.p1Name = player1Name;
            this.p2Name = player2Name;
        }

        public string GetScore()
        {
            if ((p1Score < 4 && p2Score < 4) && (p1Score + p2Score < 6))
            {
                string[] p = { "Love", "Fifteen", "Thirty", "Forty" };
                string p1Res = p[p1Score];
                return (p1Score == p2Score) ? p1Res + "-All" : p1Res + "-" + p[p2Score];
            }
            else
            {
                if (p1Score == p2Score)
                    return "Deuce";
                string playerName = p1Score > p2Score ? p1Name : p2Name;
                return ((p1Score - p2Score) * (p1Score - p2Score) == 1) ? "Advantage " + playerName : "Win for " + playerName;
            }
        }

        public void WonPoint(string playerName)
        {
            if (playerName == "player1")
                this.p1Score += 1;
            else
                this.p2Score += 1;
        }

    }
}

